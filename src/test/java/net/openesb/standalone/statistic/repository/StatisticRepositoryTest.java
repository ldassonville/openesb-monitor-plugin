package net.openesb.standalone.statistic.repository;

import java.net.ConnectException;
import java.util.Date;

import net.openesb.standalone.statistic.model.EndpointStatistics;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Statistic repository test class
 * 
 * @author ldassonville
 *
 */
public class StatisticRepositoryTest {

	private EndpointStatisticRepository repository;
	
	@Before
	public void init(){
		this.repository = new EndpointStatisticRepository();
		repository.setIndex("openesb");
		repository.setUrl("http://localhost:9200/");
	}
	
	@Test
	public void mappingTest(){
		try {
			repository.initMapping();
		}catch(Exception e){
			Assert.fail("Error saving entity");			
		}
	}

	//@Test
	public void saveTest(){
		EndpointStatistics entity =new EndpointStatistics();
		entity.setEndpoint("myTest");
		entity.setTimestamp(new Date());

		try {
			String id = repository.save(entity);
			Assert.assertNotNull("Identifier can't be null", id);
			
		}catch(ConnectException e){
			e.printStackTrace();
			Assert.fail("Error saving entity");		
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Error saving entity");
		}
		
	}
}
