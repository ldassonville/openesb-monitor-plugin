package net.openesb.standalone.statistic.repository;

import java.net.ConnectException;
import java.util.Date;

import net.openesb.standalone.statistic.model.EndpointStatistics;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.orientechnologies.orient.object.db.OObjectDatabaseTx;

/**
 * Statistic repository test class
 * 
 * @author ldassonville
 *
 */
public class LocalStorageTest {

	private  OObjectDatabaseTx db;
	
	//@Before
	public void init(){
	    db = new OObjectDatabaseTx ("memory:petshop").open("admin", "admin");

	    // REGISTER THE CLASS ONLY ONCE AFTER THE DB IS OPEN/CREATED
	    db.getEntityManager().registerEntityClasses("foo.domain");
	}
	

	//@Test
	public void saveTest(){
		EndpointStatistics entity =new EndpointStatistics();
		entity.setEndpoint("myTest");
		entity.setTimestamp(new Date());

		try {
		
		    // SAVE THE ACCOUNT: THE DATABASE WILL SERIALIZE THE OBJECT AND GIVE THE PROXIED INSTANCE
			entity = db.save( entity );
	
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail("Error saving entity");
		}
		
	}
}
