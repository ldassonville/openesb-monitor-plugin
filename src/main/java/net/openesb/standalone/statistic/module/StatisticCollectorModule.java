package net.openesb.standalone.statistic.module;

import net.openesb.standalone.statistic.repository.EndpointRepository;
import net.openesb.standalone.statistic.repository.EndpointStatisticRepository;
import net.openesb.standalone.statistic.repository.JVMStatisticRepository;
import net.openesb.standalone.statistic.spi.StatisticsCollectorService;
import net.openesb.standalone.statistic.workers.EndpointStatisticCollectorWorker;
import net.openesb.standalone.statistic.workers.JVMStatisticCollectorWorker;
import net.openesb.standalone.statistic.workers.SynchronizerWorker;

import com.google.inject.AbstractModule;

/**
 *
 * @author ldassonville
 */
public class StatisticCollectorModule extends AbstractModule {

    @Override
    protected void configure() {
    	
        bind(StatisticsCollectorService.class).asEagerSingleton();
        
        //Collect workers
        bind(EndpointStatisticCollectorWorker.class);
        bind(JVMStatisticCollectorWorker.class);
        bind(SynchronizerWorker.class);
        
        //Elasticsearch repositories
        bind(EndpointStatisticRepository.class);
        bind(JVMStatisticRepository.class);
        bind(EndpointRepository.class);

    }
}
