package net.openesb.standalone.statistic.module;

import net.openesb.management.api.AdministrationService;
import net.openesb.management.api.ComponentService;
import net.openesb.management.api.ConfigurationService;
import net.openesb.management.api.EndpointService;
import net.openesb.management.api.JvmMetricsService;
import net.openesb.management.api.ManagementFactory;
import net.openesb.management.api.MessageService;
import net.openesb.management.api.ServiceAssemblyService;
import net.openesb.management.api.SharedLibraryService;
import net.openesb.management.api.StatisticsService;

import com.google.inject.AbstractModule;

/**
 *
 * @author Loic DASSONVILLE
 * @author OpenESB Community
 */
public class ManagementModule extends AbstractModule {


    @Override
    protected void configure() {
    	bind(AdministrationService.class).toInstance(ManagementFactory.getAdministrationService());
        bind(ComponentService.class).toInstance(ManagementFactory.getComponentService());
        bind(ConfigurationService.class).toInstance(ManagementFactory.getConfigurationService());
        bind(EndpointService.class).toInstance(ManagementFactory.getEndpointService());
        bind(MessageService.class).toInstance(ManagementFactory.getMessageService());
        bind(ServiceAssemblyService.class).toInstance(ManagementFactory.getServiceAssemblyService());
        bind(StatisticsService.class).toInstance(ManagementFactory.getStatisticsService());
        bind(SharedLibraryService.class).toInstance(ManagementFactory.getSharedLibraryService());
        bind(JvmMetricsService.class).toInstance(ManagementFactory.getJvmMetricsService());
    }
}
