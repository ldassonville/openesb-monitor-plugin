package net.openesb.standalone.statistic.model;

import java.util.Date;
import java.util.Map;

/**
 * Abstract statistics 
 * 
 * @author ldassonville
 */
public abstract class AbstractStatistics {

	/** Request date **/
	private Date timestamp;

	/** Collect duration **/
	private Long duration;
	
	/** OpenEsb node name **/
	private String nodeName;
	
	/** Server name **/
	private String hostname;

	/** Statistics values **/
	private Map<String, Object> statistics;

	public AbstractStatistics() {
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
	
	public String getNodeName() {
		return nodeName;
	}

	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}

	public Map<String, Object> getStatistics() {
		return statistics;
	}

	public void setStatistics(Map<String, Object> statistics) {
		this.statistics = statistics;
	}

}
