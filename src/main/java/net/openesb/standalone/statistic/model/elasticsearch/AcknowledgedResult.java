package net.openesb.standalone.statistic.model.elasticsearch;

public class AcknowledgedResult extends ErrorResult {
	
	private Boolean acknowledged;

	public Boolean getAcknowledged() {
		return acknowledged;
	}

	public void setAcknowledged(Boolean acknowledged) {
		this.acknowledged = acknowledged;
	}
}
