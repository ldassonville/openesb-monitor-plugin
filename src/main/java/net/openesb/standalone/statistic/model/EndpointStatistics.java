package net.openesb.standalone.statistic.model;


/**
 * Endpoint statistics 
 * 
 * @author ldassonville
 */
public class EndpointStatistics extends AbstractStatistics{

	/** Endpoint name **/
	private String endpoint;

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}
}
