package net.openesb.standalone.statistic.model.elasticsearch;

/**
 * Default elasticsearch error result.
 * 
 * @author ldassonville
 * 
 */
public class ErrorResult {
	
	private Integer status;
	private String error;
	
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
}
