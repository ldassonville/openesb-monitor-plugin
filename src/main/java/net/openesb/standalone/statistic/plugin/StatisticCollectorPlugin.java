package net.openesb.standalone.statistic.plugin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import net.openesb.standalone.Lifecycle;
import net.openesb.standalone.plugins.Plugin;
import net.openesb.standalone.statistic.module.ManagementModule;
import net.openesb.standalone.statistic.module.StatisticCollectorModule;
import net.openesb.standalone.statistic.spi.StatisticsCollectorService;

import com.google.inject.Module;

/**
 * Statistic collector service.
 * 
 * @author ldassonville
 *
 */
public class StatisticCollectorPlugin implements Plugin {
	
	/**
	 * {@inheritDoc}
	 */
	public String name() {
		return "Statistic collector";
	}

	/**
	 * {@inheritDoc}
	 */
	public String description() {
		return "Collect node statistics and save it in elasticsearch";
	}

	/**
	 * {@inheritDoc}
	 */
	public Collection<Class<? extends Module>> modules() {
		List<Class<? extends Module>> modules = new ArrayList<Class<? extends Module>>();
		modules.add(ManagementModule.class);
		modules.add(StatisticCollectorModule.class);
		return modules;
	}

	/**
	 * {@inheritDoc}
	 */
	public Collection<Class<? extends Lifecycle>> services() {
		List<Class<? extends Lifecycle>> services = new ArrayList<Class<? extends Lifecycle>>();
		services.add(StatisticsCollectorService.class);
		return services;
	}
	    
}