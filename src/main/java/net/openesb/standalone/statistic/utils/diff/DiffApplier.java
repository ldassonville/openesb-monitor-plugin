package net.openesb.standalone.statistic.utils.diff;

import net.openesb.model.api.metric.Metric;

public interface DiffApplier<T> {

	/**
	 * Give the difference between values
	 * @param val1
	 * @param val2
	 * @return
	 */
	T diff(String key, Metric val1, Metric val2);
	
	/**
	 * Respond true if difference is appliable for this classes
	 * @param object
	 * @return
	 */
	boolean isSupported(Metric clazz);
}
