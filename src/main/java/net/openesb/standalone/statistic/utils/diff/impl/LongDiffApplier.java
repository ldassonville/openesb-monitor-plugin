package net.openesb.standalone.statistic.utils.diff.impl;

import net.openesb.model.api.metric.Gauge;
import net.openesb.model.api.metric.Metric;
import net.openesb.standalone.statistic.utils.diff.DiffApplier;

public class LongDiffApplier implements DiffApplier<Long> {

	@SuppressWarnings("rawtypes")
	public boolean isSupported(Metric metric){
		if(metric instanceof Gauge<?>){
			return ((Gauge)metric).getValue() instanceof Long;
		}
		return false;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public Long diff(String key, Metric currentMetric, Metric previousMetric) {
		if(key.contains("Time")){
			return ((Gauge<Long>)currentMetric).getValue();
		}
		
		return  ((Gauge<Long>)currentMetric).getValue() -  ((Gauge<Long>)previousMetric).getValue();
	}

}
