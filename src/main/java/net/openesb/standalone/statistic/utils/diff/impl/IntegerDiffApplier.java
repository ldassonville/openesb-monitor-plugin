package net.openesb.standalone.statistic.utils.diff.impl;

import net.openesb.model.api.metric.Gauge;
import net.openesb.model.api.metric.Metric;
import net.openesb.standalone.statistic.utils.diff.DiffApplier;

public class IntegerDiffApplier implements DiffApplier<Integer> {


	@SuppressWarnings("rawtypes")
	public boolean isSupported(Metric metric) {
		if (metric instanceof Gauge<?>) {
			return ((Gauge) metric).getValue() instanceof Long;
		}
		return false;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Integer diff(String metricName, Metric currentMetric, Metric previousMetric) {
		if(metricName.contains("Time")){
			return ((Gauge<Integer>)currentMetric).getValue();
		}
		return ((Gauge<Integer>) currentMetric).getValue()- ((Gauge<Integer>) previousMetric).getValue();
	}

}
