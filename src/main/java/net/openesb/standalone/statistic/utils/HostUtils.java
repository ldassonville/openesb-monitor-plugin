package net.openesb.standalone.statistic.utils;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Logger;

public class HostUtils {
	
	/** 
	 * Default Logger
	 */
	private static final Logger LOG = Logger.getLogger(HostUtils.class.getPackage().getName());

	
	public static String hostname;
	
	public static String getHostname(){
		
		if(hostname != null){
			return hostname;
		}
		
		try {
			hostname = InetAddress.getLocalHost().getHostName();
			return hostname;
		} catch (UnknownHostException e) {
			LOG.warning("Error resolving hostname" );
			return null;
		}	
	}
	

}
