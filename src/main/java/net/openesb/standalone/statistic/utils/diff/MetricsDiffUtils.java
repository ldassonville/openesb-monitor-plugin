package net.openesb.standalone.statistic.utils.diff;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.openesb.model.api.metric.Metric;
import net.openesb.standalone.statistic.utils.diff.impl.IntegerDiffApplier;
import net.openesb.standalone.statistic.utils.diff.impl.LongDiffApplier;

import com.google.common.collect.Sets;

public class MetricsDiffUtils {

	/**
	 * Applier Cache
	 */
	public Map<String, DiffApplier<?>> diffMetricCache = new HashMap<String, DiffApplier<?>>();
	
	public static List<DiffApplier<?>> diffApplyers =new ArrayList<DiffApplier<?>>();
	static{
		diffApplyers.add(new LongDiffApplier());
		diffApplyers.add(new IntegerDiffApplier());
		
	}
	
	/**
	 * Return the differences between the metrics.
	 * 
	 * @param previous
	 * @param current
	 * @return
	 */
	public Map<String, Object> getDifferences( Map<String, Metric> previous,  Map<String, Metric> current){
		
		Map<String, Object> res = new HashMap<String, Object>();
		
		Set<String> keys = Sets.intersection(previous.keySet(), current.keySet());
		
		for (String metricName : keys) {
			DiffApplier<?> diffApplyer =  resolveDiffApplyer(metricName, previous.get(metricName));
			
			//If difference applier resolved
			if(diffApplyer != null){	
				res.put(metricName, diffApplyer.diff(metricName, current.get(metricName),previous.get(metricName)));
			}
		}
		return res;
	}

	/**
	 * Resolve the diffApplyer use to the metric
	 * 	
	 * @param metricName
	 * @param metric
	 * @return
	 */
	public DiffApplier<?> resolveDiffApplyer(String metricName,  Metric metric){
			
		if(diffMetricCache.containsKey(metricName)){
			return diffMetricCache.get(metricName);
		}
		
		for (DiffApplier<?> diffApplier : diffApplyers) {
			if(metricName != null && diffApplier.isSupported(metric)){
				diffMetricCache.put(metricName, diffApplier);
				return diffApplier;
			}
		}

		return null;
	}
	
}
