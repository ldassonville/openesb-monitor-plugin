package net.openesb.standalone.statistic.spi;

import java.util.Calendar;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import net.openesb.standalone.Lifecycle;
import net.openesb.standalone.LifecycleException;
import net.openesb.standalone.http.HttpServer;
import net.openesb.standalone.statistic.rest.api.WemblyApplication;
import net.openesb.standalone.statistic.workers.EndpointStatisticCollectorWorker;
import net.openesb.standalone.statistic.workers.JVMStatisticCollectorWorker;
import net.openesb.standalone.statistic.workers.SynchronizerWorker;

import com.google.inject.Inject;

/**
 * 
 * @author ldassonville
 * 
 */
public class StatisticsCollectorService implements Lifecycle {

	private static final Logger LOG = Logger.getLogger(StatisticsCollectorService.class.getPackage().getName());

	private ScheduledExecutorService scheduledThreadPool;
	
	@Inject
	private HttpServer httpServer;
	
	@Inject
	private EndpointStatisticCollectorWorker endpointWorker;
	
	@Inject
	private JVMStatisticCollectorWorker memoryWorker;
	
	@Inject
	private SynchronizerWorker syncWorker;

	@Override
	public void start() throws LifecycleException {

		LOG.info("StatisticsCollectorService starting");
		
		registerRestAPI();
		
		this.scheduledThreadPool = Executors.newScheduledThreadPool(1);

		// Worker initialization
		endpointWorker.init();
		memoryWorker.init();
		syncWorker.init();
		
		// Launching schedule
		int delay = 60 - Calendar.getInstance().get(Calendar.SECOND);
		scheduledThreadPool.scheduleAtFixedRate(endpointWorker, delay, 60, TimeUnit.SECONDS);
		scheduledThreadPool.scheduleAtFixedRate(memoryWorker, delay, 60, TimeUnit.SECONDS);
		scheduledThreadPool.scheduleAtFixedRate(syncWorker, delay, 60, TimeUnit.SECONDS);
		
		LOG.info("StatisticsCollectorService started");
	}

	@Override
	public void stop() throws LifecycleException{
		scheduledThreadPool.shutdownNow();
		LOG.info("StatisticsCollectorService stopped");
	}

	
	private void registerRestAPI(){
		LOG.info("Registering wembly rest api");	
		httpServer.addRestHandler(new WemblyApplication(), "/wembly");	
	}
}
