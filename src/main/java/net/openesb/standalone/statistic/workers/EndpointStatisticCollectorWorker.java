package net.openesb.standalone.statistic.workers;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.inject.Inject;

import net.openesb.management.api.EndpointService;
import net.openesb.management.api.ManagementException;
import net.openesb.management.api.StatisticsService;
import net.openesb.model.api.Endpoint;
import net.openesb.model.api.metric.Metric;
import net.openesb.standalone.node.Node;
import net.openesb.standalone.settings.Settings;
import net.openesb.standalone.statistic.model.EndpointStatistics;
import net.openesb.standalone.statistic.repository.EndpointRepository;
import net.openesb.standalone.statistic.repository.EndpointStatisticRepository;
import net.openesb.standalone.statistic.spi.StatisticsCollectorService;
import net.openesb.standalone.statistic.utils.HostUtils;
import net.openesb.standalone.statistic.utils.diff.MetricsDiffUtils;

/**
 * Collect endpoints statistics from openESB node and save it in elasticsearch.
 * 
 * @author ldassonville
 * 
 */
public class EndpointStatisticCollectorWorker implements Runnable {

	protected static final String DEFAULT_ELASTICSEARCH_URL = "http://localhost:9200";
	
	/** 
	 * Default Logger
	 */
	private static final Logger LOG = Logger.getLogger(StatisticsCollectorService.class.getPackage().getName());

	/**
	 * Node settings
	 */
	@Inject
	private Node node;
	
	/**
	 * Node settings
	 */
	@Inject
	private Settings settings;

	/**
	 * OpenEsb statistic service
	 */
	@Inject
	private StatisticsService statisticService;

	/**
	 * OpenEsb endpoint service
	 */
	@Inject
	private EndpointService endpointService;
	
	/**
	 * Previous endpoint map
	 */
	private final Map<String, Map<String, Metric>> previousEndpoints;
	
	/**
	 * Default metric differences computing engine 
	 */
	private final MetricsDiffUtils diffUtils = new MetricsDiffUtils();
	
	/**
	 * Elasticsearch statistic repository
	 */
	@Inject
	private EndpointStatisticRepository statisticRepository;
	
	/**
	 * Elasticsearch endpoint repository
	 */
	@Inject
	private EndpointRepository endpointRepository;
		
	/**
	 * Initialize worker.
	 * 
	 * @param url
	 *            elasticsearch URL
	 */
	public EndpointStatisticCollectorWorker() {
		this.previousEndpoints = new HashMap<String, Map<String, Metric>>();	
	}

	/**
	 * Initialize worker
	 */
	public void init() {
		statisticRepository.setUrl(settings.get("elasticsearch.url", DEFAULT_ELASTICSEARCH_URL));
	}

	@Override
	public void run() {

		try {
			LOG.info("Starting endpoints statistics collecte");
			
			Set<Endpoint> endpoints = endpointService.findEndpoints(null, false);
			for (Endpoint endpoint : endpoints) {
					
				String endpointName = endpoint.getName();
				
				LOG.info("Processing endpoint : "+endpointName);	
				//endpointRepository.save(endpoint);
			
				LOG.info("Resolving statistics for : "+endpointName);
				Map<String, Metric> statistics = statisticService.getEndpointStatistics(endpointName);
				
				// Recording endpoint statistics
				if (previousEndpoints.containsKey(endpointName)) {

					EndpointStatistics ponctualEndpoint = new EndpointStatistics();
					ponctualEndpoint.setNodeName(node.name());
					ponctualEndpoint.setTimestamp(new Date());
					ponctualEndpoint.setEndpoint(endpoint.getName());
					ponctualEndpoint.setHostname(HostUtils.getHostname());
					ponctualEndpoint.setStatistics(diffUtils.getDifferences(previousEndpoints.get(endpointName), statistics));

					statisticRepository.save(ponctualEndpoint);
				}
				// Saving previous state
				previousEndpoints.put(endpointName, statistics);
			}
			LOG.info("Endpoints statistics collecte done");
			
		} catch (ManagementException e) {
			LOG.severe("Management Error resolving endpoints " + e.getMessage());
		} catch (Exception e) {
			LOG.severe("Error resolving endpoints. " + e.getMessage());
		}
	}
}
