package net.openesb.standalone.statistic.workers;

import java.util.logging.Logger;

/**
 * Synchronize disconnected local data with remote storage.
 * 
 * @author ldassonville
 * 
 */
public class SynchronizerWorker implements Runnable {

	private static final Logger LOG = Logger.getLogger(SynchronizerWorker.class.getPackage().getName());

	/**
	 * Default worker initiation
	 */
	public SynchronizerWorker() {
	}
	
	/**
	 * Initialize worker
	 */
	public void init() {
	}

	@Override
	public void run() {

		try {
			LOG.info("Running synchronisation");
		
			LOG.info("Synchronisation done");
			
		} catch (Exception e) {
			LOG.severe("Error synchronizing data" + e.getMessage());
		}
	}

}
