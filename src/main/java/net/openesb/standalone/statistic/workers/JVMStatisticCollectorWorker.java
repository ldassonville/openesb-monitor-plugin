package net.openesb.standalone.statistic.workers;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.inject.Inject;

import net.openesb.management.api.JvmMetricsService;
import net.openesb.model.api.metric.Gauge;
import net.openesb.model.api.metric.Metric;
import net.openesb.standalone.node.Node;
import net.openesb.standalone.settings.Settings;
import net.openesb.standalone.statistic.model.JVMStatistics;
import net.openesb.standalone.statistic.repository.JVMStatisticRepository;
import net.openesb.standalone.statistic.utils.HostUtils;

/**
 * Collect endpoints statistics from openESB node and save it in elasticsearch.
 * 
 * @author ldassonville
 * 
 */
public class JVMStatisticCollectorWorker implements Runnable {

	protected static final String DEFAULT_ELASTICSEARCH_URL = "http://localhost:9200";
	
	/** 
	 * Default Logger
	 */
	private static final Logger LOG = Logger.getLogger(JVMStatisticCollectorWorker.class.getPackage().getName());

	/**
	 * Node settings
	 */
	@Inject
	private Node node;
	
	/**
	 * Node settings
	 */
	@Inject
	private Settings settings;

	/**
	 * OpenEsb statistic service
	 */
	@Inject
	private JvmMetricsService jvmService;
	
	@Inject
	private JVMStatisticRepository jvmStatisticRepository;
	
	/**
	 * Initialize worker.
	 * 
	 * @param url
	 *            elasticsearch URL
	 */
	public JVMStatisticCollectorWorker() {
	}

	/**
	 * Initialize worker
	 */
	public void init() {
		jvmStatisticRepository.setUrl(settings.get("elasticsearch.url", DEFAULT_ELASTICSEARCH_URL));
	}

	@Override
	public void run() {

		try {
			LOG.info("Starting memory statistics collecte");
			
			
			Map<String, Metric> mapMemoryUsage = jvmService.getMemoryUsage();
			
			JVMStatistics jvmStatistics = new JVMStatistics();
			Map<String, Object> data = new HashMap<String, Object>();
			jvmStatistics.setTimestamp(new Date());
			jvmStatistics.setNodeName(node.name());
			jvmStatistics.setStatistics(data);
			jvmStatistics.setHostname(HostUtils.getHostname());
			
			
			for (Map.Entry<String, Metric> entry : mapMemoryUsage.entrySet()) {
				if(entry.getValue() instanceof  Gauge) {
					Gauge<?> gauge = (Gauge<?>)entry.getValue();
					data.put(entry.getKey(), gauge.getValue());
				}
			}
			
			jvmStatisticRepository.save(jvmStatistics);
			
			
			Map<String, Metric> mapGarbageCollector = jvmService.getGarbageCollector();
			LOG.info("Memory statistics collecte done");

		} catch (Exception e) {
			LOG.severe("Error resolving endpoints. " + e.getMessage());
		}
	}
}
