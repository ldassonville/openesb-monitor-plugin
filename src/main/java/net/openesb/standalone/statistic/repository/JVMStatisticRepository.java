package net.openesb.standalone.statistic.repository;

import net.openesb.standalone.statistic.model.JVMStatistics;
import net.openesb.standalone.statistic.repository.annotation.Type;

/**
 * Statistic repository.
 * 
 * @author ldassonville
 *
 */
@Type("jvm-statistic")
public class JVMStatisticRepository extends AbstractRepository<JVMStatistics> {

}
