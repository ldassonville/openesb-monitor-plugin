package net.openesb.standalone.statistic.repository;

import java.io.InputStream;
import java.util.logging.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import net.openesb.standalone.statistic.model.elasticsearch.UpdateResult;
import net.openesb.standalone.statistic.repository.annotation.Type;
import net.openesb.standalone.statistic.spi.StatisticsCollectorService;

import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.glassfish.jersey.jackson.JacksonFeature;

/**
 * Abstract elasticsearch repository.
 * 
 * @author ldassonville
 *
 * @param <E> Entity class
 */
public abstract class AbstractRepository<E> {

	/** Elasticsearch mapping URL suffix **/
	private final static String MAPPING_SUFFIX = "/_mapping";	
	
	/** Elasticsearch URL **/
	private String url;
	
	/** Elasticsearch index name **/
	private String index = "openesb";
	
	/** Object type **/
	private String type;
	
	/** Default Logger **/
	private static final Logger LOG = Logger.getLogger(StatisticsCollectorService.class.getPackage().getName());
	
	/**
	 * Default constructor
	 */
	public AbstractRepository(){
		this.type = resolveType();
	}
	
	/**
	 * Setter for elasticsearch REST API URL
	 * 
	 * @param url URL
	 */
	public void setUrl(String url) {
		this.url = url;
	}
		
	/**
	 * Setter for elasticsearch Index name
	 * 
	 * @param index
	 */
	public void setIndex(String index) {
		this.index = index;
	}

	/**
	 * Give the default entity name.
	 * 
	 * @return
	 */
	protected String resolveType(){
		Type type = getClass().getAnnotation(Type.class);
		return type.value();
	}
	
	/**
	 * Return service Rest client.
	 * 
	 * @return client
	 */
	protected Client getClient(){
		Client client = ClientBuilder.newClient();
		client.register(JacksonFeature.class);
		return client;
	}
	
	/**
	 * Return the default entity web target.
	 * 
	 * @return
	 */
	protected WebTarget getWebTarget(){	
		return getWebTarget(index+ "/" + this.type);
	}
	
	/**
	 * Return a web target for the given URL.
	 * 
	 * @param path
	 * @return
	 */
	protected WebTarget getWebTarget(final String path){
		String resourcePath = path.startsWith("/") ? path : "/"+path;
		return getClient().target(url).path(resourcePath);
	}
	
	/**
	 * Return response as JSON node.
	 * 
	 * @param response response to parse.
	 * @return
	 */
	protected JsonNode getJsonResponse(Response response){
		try{
			String strResponse = response.readEntity(String.class);
			ObjectMapper mapper = new ObjectMapper();
			return mapper.readTree(strResponse);
			
		} catch (Exception e) {
			LOG.severe(e.getMessage());
			throw new RuntimeException("JSON Parse failure");
		}
	}
	
	
	/**
	 * Initialize elasticsearch index.
	 */
	protected void initIndex() {
		LOG.info("Initializing index "+index);
		WebTarget target = getWebTarget(index);
		Response response = target.request().put(Entity.text(""));
		JsonNode jsonNode = getJsonResponse(response);
	}
	
	
	/**
	 * Initialize elasticsearch resource mapping.
	 *  
	 */
	public void initMapping(){
		LOG.info("Start mapping initialisation for type "+this.type);
		
		InputStream inputStream = null;

		try {
			initIndex();
			
			//Resolving mapping configuration
			inputStream = getClass().getResourceAsStream("/mapping/" + this.type + ".json");
			
			//Creating index mapping
			WebTarget target = getWebTarget(index +"/"+ this.type + MAPPING_SUFFIX);
			Builder builder = target.request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON);
			Response response = builder.put(Entity.entity(IOUtils.toString(inputStream), MediaType.APPLICATION_JSON));
			
			String strResponse = response.readEntity(String.class);
			LOG.fine(strResponse);

		} catch (Exception e) {
			LOG.severe(e.getMessage());
		} finally {
			IOUtils.closeQuietly(inputStream);
		}
	}
	
	/**
	 * Save resource entity in elasticsearch.
	 * 
	 * @param entity 
	 * 				entity to save
	 * @return identifier
	 */
	public String save(E entity) throws Exception{
		
		LOG.fine("Saving "+this.type+" entity");
		
		//Saving entity
		WebTarget target = getWebTarget();
		Builder builder = target.request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON);
		Response response = builder.post(Entity.entity(entity, MediaType.APPLICATION_JSON));
		
		UpdateResult updateResult = response.readEntity(UpdateResult.class);
		if(updateResult.getStatus() != null){
			//TODO
		}
		
		LOG.fine("Entity saved with id "+updateResult.get_id());
		return updateResult.get_id();
	}
}
