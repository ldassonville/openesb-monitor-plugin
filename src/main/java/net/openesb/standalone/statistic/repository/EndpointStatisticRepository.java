package net.openesb.standalone.statistic.repository;

import net.openesb.standalone.statistic.model.EndpointStatistics;
import net.openesb.standalone.statistic.repository.annotation.Type;

/**
 * Statistic repository.
 * 
 * @author ldassonville
 *
 */
@Type("endpoint-statistic")
public class EndpointStatisticRepository extends AbstractRepository<EndpointStatistics> {

}
