package net.openesb.standalone.statistic.repository;

import net.openesb.model.api.Endpoint;
import net.openesb.standalone.statistic.repository.annotation.Type;

/**
 * Statistic repository.
 * 
 * @author ldassonville
 *
 */
@Type("endpoint")
public class EndpointRepository extends AbstractRepository<Endpoint> {

}
