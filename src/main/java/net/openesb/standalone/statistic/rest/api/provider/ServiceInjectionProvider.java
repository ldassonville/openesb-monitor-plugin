package net.openesb.standalone.statistic.rest.api.provider;

import org.glassfish.hk2.utilities.binding.AbstractBinder;

/**
 *
 * @author Loic DASSONVILLE (ldassonville.openesb at gmail.com)
 * @author OpenESB Community
 */
public class ServiceInjectionProvider extends AbstractBinder {
	
    @Override
    protected void configure() {

    }
    
}
