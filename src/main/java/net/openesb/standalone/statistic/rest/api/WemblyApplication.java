package net.openesb.standalone.statistic.rest.api;

import net.openesb.rest.api.provider.CrossDomainFilter;
import net.openesb.standalone.statistic.rest.api.provider.ObjectMapperProvider;
import net.openesb.standalone.statistic.rest.api.provider.ServiceInjectionProvider;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;

/**
 * 
 * @author Loic DASSONVILLE (ldassonville.openesb at gmail.com)
 * @author OpenESB Community
 */
public class WemblyApplication extends ResourceConfig {

	public WemblyApplication() {
		super(

				// Register Jackson ObjectMapper resolver
				ObjectMapperProvider.class, 
                
                // Register filter
                // BasicAuthenticationFilter.class,
                CrossDomainFilter.class,
                
				//Jersey features
				JacksonFeature.class);

		register(new ServiceInjectionProvider());
	}
}
